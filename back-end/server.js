var express = require('express');
var app = express();
var server = require('http').Server(app);
var SerialPort = require('serialport');

var port = new SerialPort('/dev/cu.usbmodem14611', {
  baudrate: 9600,
  parser: SerialPort.parsers.readline("\n")
}, (err, data) => {
  if (err) {
    console.error('There was an error opening port!', err);
  }
})

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var getData = [];

port.on('open', onOpen);
port.on('data', onData);

function onData (dato) {
  const splitString = dato.split(',');
  var d = new Date();

  var dateTime = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes();

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("temperature");

    var myobj = {
      objC: splitString[1],
      objF: splitString[3],
      ambC: splitString[0],
      ambF: splitString[2],
      dateTime: dateTime,
    };

    dbo.collection("temperatures").insertOne(myobj, function(err, res) {
      if (err) throw err;
      console.log("temperature inserted");
      db.close();
    });

    app.get('/api/temperature', (req, res) => {
      res.send({ express: getData });
    });

    getTemperatureData();
  });
}

function getTemperatureData() {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("temperature");
    dbo.collection("temperatures").find({}).toArray(function(err, result) {
      if (err) throw err;
      getData = result;
      console.log("getData: ");
      console.log(getData);
      db.close();
    });
    app.get('/api/temperature', (req, res) => {
      res.send({ express: getData });
    });
  });
}

function onOpen() {
  console.log("Arduino conected");
}

server.listen(8000, function() {
  console.log("Server connected");
  getTemperatureData();
})
