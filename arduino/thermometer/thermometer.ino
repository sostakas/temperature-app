#include <Wire.h>
#include <Adafruit_MLX90614.h>
Adafruit_MLX90614 mlx = Adafruit_MLX90614();
#include <Time.h>  
//time_t t = now();
void setup() {
  Serial.begin(9600);
//  Serial.println("Adafruit MLX90614 test");  
  mlx.begin();  
}
void loop() {
//  Serial.println();
  Serial.print(mlx.readAmbientTempC()); 
  Serial.print(","); 
  Serial.print(mlx.readObjectTempC());
  Serial.print(",");
  Serial.print(mlx.readAmbientTempF()); 
  Serial.print(",");
  Serial.print(mlx.readObjectTempF());
  
  Serial.println();
  delay(1000);
}

//void digitalClockDisplay(){
//  // digital clock display of the time
//  Serial.print(hour());
//  printDigits(minute());
//  printDigits(second());
//  Serial.print(" ");
//  Serial.print(day());
//  Serial.print(" ");
//  Serial.print(month());
//  Serial.print(" ");
//  Serial.print(year()); 
//  Serial.println(); 
//}
