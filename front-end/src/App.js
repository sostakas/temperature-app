import React, { Component } from 'react';
import { LineChart } from 'react-chartkick'
import Chart from 'chart.js'

class App extends Component {
  constructor() {
    super();
    this.state = {
      response: '',
      temps: [],
      objC: [],
      objF: [],
      ambC: [],
      ambF: [],
      dateTime: [],
    };
  }

  componentDidMount() {
    this.getTempData();
  }

  getTempData() {
    setInterval(() => {
      this.state.temps = this.callApi()
        .then(res => this.setState({temps: res.express}))
        .catch(function(err) {
            return err;
        });
    }, 1000)
  }

  render() {
    let obj = {}

    this.state.temps.map((e, i) => {
      obj[e.dateTime] = e.ambC;
      return (
          <li key={e._id}>{e.dateTime} {e.objC} {e.objF}</li>
      )
    })

    return (
      <div>
        <LineChart data={obj} />
      </div>
    );
  }

  callApi = async() => {
    const response = await fetch('/api/temperature');
    const body = await response.json();
    this.state.temps = body;
    if(response.status !== 200) throw Error(body.message);
    return body;
  };
}
export default App;
